import java.util.concurrent.ThreadLocalRandom;      //used to generate random int
import java.util.Scanner;

public class NumGuess{

    public static void main(String[] args){
        Scanner reader = new Scanner(System.in);
        int randInt = ThreadLocalRandom.current().nextInt(1,10);     //generates random int between 1-10
        boolean isInt=false;
        int intGuess=0;
        System.out.println("Guess a number between 1 & 10:");
        String strGuess = "";
        do{
            try{                                            //checks if String entered is an integer
                strGuess=reader.nextLine();                 //collects int that user inputs as a string
                intGuess = Integer.parseInt(strGuess);      //converts string collected into an integer
                isInt=true;
            }catch(NumberFormatException e){
                System.out.println("Sorry "+strGuess+" is not an integer. Please enter an integer:");
            }
        }while(isInt==false);       //continues loop until boolean "isInt" is false
        if (randInt!=intGuess){
            do{
                if(randInt>intGuess){               //if user's int is less than the randInt
                    System.out.println("Sorry you have guessed the wrong number. " +
                            "The correct number is greater than the number you have entered.");
                    System.out.println("Please enter another guess:");
                    strGuess=reader.nextLine();
                    intGuess = Integer.parseInt(strGuess);          //This can be done in one line intGuess=Integer.parseInt(reader.nextLine());

                } else if (randInt<intGuess) {      //if user's int is greater than the randInt
                    System.out.println("Sorry you have guessed the wrong number. " +
                            "The correct number is less than the number you have entered.");
                    System.out.println("Please enter another guess:");
                    strGuess=reader.nextLine();
                    intGuess = Integer.parseInt(strGuess);

                }
            }
            while(randInt!=intGuess);       //loop continues until the user's int is equal to the randInt
        }

        if(randInt==intGuess){
            System.out.println("The number was "+randInt);
            System.out.println("Congrats, you have guessed the correct number!!");
        }
    }
}
