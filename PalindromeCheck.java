import java.util.Scanner;

public class Main {
    public static void main(String[] args){
        Scanner reader = new Scanner(System.in);
        System.out.println("What is the word you would like to check for a palindrome?");
        String palindrome = reader.nextLine();

        boolean isPalin = checkPalindrome(palindrome);
        if(isPalin == true){
            System.out.println("Congrats!! The word you entered is a palindrome!");
        }else{
            System.out.println("Sorry, unfortunately the word you entered is not a palindrome.");
        }
    }
    public static boolean checkPalindrome(String word){
        String wordSwaped="";
        boolean ans=false;
        for(int i=word.length()-1; i>=0;i--){
            wordSwaped+=word.charAt(i);
        }
        if(word.equals(wordSwaped))
            ans=true;

            return ans;
    }

}
